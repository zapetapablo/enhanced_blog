#
from django.urls import path

from . import views

app_name = "users_app"

urlpatterns = [
    path(
        'register/', 
        views.UserRegisterView.as_view(),
        name='user-register',
    ),
    path(
        'login/', 
        views.LoginUser.as_view(),
        name='user-login',
    ),
    path(
        'logout/', 
        views.LogoutView.as_view(),
        name='user-logout',
    ),
    path(
        'update/<pk>/', 
        views.UpdatePasswordView.as_view(),
        name='user-update-password',
    ),
    path(
        'updateUser/<pk>/',
        views.UpdateUserView.as_view(),
        name='basic_information_user_update'
    ),
    path(
        'incorrect!!/',
        views.IconcorrectPassword.as_view(),
        name = "incorrect-password"
    ),
    path(
        'contraseñaOlvidada/',
        views.RecoverPasswordListView.as_view(),
        name='forgot-password'
    )
]