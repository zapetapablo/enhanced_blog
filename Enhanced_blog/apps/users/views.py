from django.shortcuts import render
from django.core.mail import send_mail
from django.urls import reverse_lazy, reverse
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponseRedirect, HttpResponse

from django.views.generic import (
    View,
    CreateView,
    UpdateView,
    TemplateView,
    ListView
)

from django.views.generic.edit import (
    FormView
)

from .forms import (
    UserRegisterForm, 
    LoginForm,
    UpdatePasswordForm,
)
#
from .models import User
# 


class UserRegisterView(FormView):
    template_name = 'users/register.html'
    form_class = UserRegisterForm
    success_url = reverse_lazy('users_app:user-login')

    def form_valid(self, form):
        #
        User.objects.create_user(
            form.cleaned_data['email'],
            form.cleaned_data['password1'],
            full_name=form.cleaned_data['full_name'],
            ocupation=form.cleaned_data['ocupation'],
            genero=form.cleaned_data['genero'],
            date_birth=form.cleaned_data['date_birth'],
        )
        # enviar el codigo al email del user
        return super(UserRegisterView, self).form_valid(form)



class LoginUser(FormView):
    template_name = 'users/login.html'
    form_class = LoginForm
    success_url = reverse_lazy('favorites_app:profile')

    def form_valid(self, form):
        user = authenticate(
            email=form.cleaned_data['email'],
            password=form.cleaned_data['password']
        )
        login(self.request, user)
        return super(LoginUser, self).form_valid(form)


class LogoutView(View):

    def get(self, request, *args, **kargs):
        logout(request)
        return HttpResponseRedirect(
            reverse(
                'users_app:user-login'
            )
        )


class UpdatePasswordView(LoginRequiredMixin, FormView):
    template_name = 'users/update.html'
    form_class = UpdatePasswordForm
    success_url = reverse_lazy('users_app:user-login')
    login_url = reverse_lazy('users_app:user-login')

    def form_valid(self, form):
        usuario = self.request.user
        user = authenticate(
            email=usuario.email,
            password=form.cleaned_data['user_password']
        )

        if user:
            new_password = form.cleaned_data['new_password2']
            usuario.set_password(new_password)
            usuario.save()
        else:
            return HttpResponseRedirect(
                reverse("users_app:incorrect-password")
            )

        logout(self.request)
        return super(UpdatePasswordView, self).form_valid(form)

class UpdateUserView(LoginRequiredMixin, UpdateView):
    template_name = "favorites/updateUser.html"
    model = User
    login_url = reverse_lazy('users_app:user-login')
    
    fields = [
        'avatar_name',
        'avatar_image',
        'full_name',
        'ocupation',
        'genero'
    ]
    success_url = reverse_lazy('favorites_app:profile')

    def form_valid(self, form):
        return super(UpdateUserView, self).form_valid(form)

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super(UpdateUserView, self).post(request, *args, **kwargs)


class IconcorrectPassword(TemplateView):
    template_name = "users/incorrectPassword.html"

class RecoverPasswordListView(TemplateView):
    template_name = "users/incorrectPassword.html"

        
   
 