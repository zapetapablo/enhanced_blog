from django.db import models
from django.db.models.functions import Right, Left
#
from django.contrib.auth.models import BaseUserManager

class UserManager(BaseUserManager, models.Manager):

    def _create_user(self, email, password, is_staff, is_superuser, is_active, **extra_fields):
        user = self.model(
            email=email,
            is_staff=is_staff,
            is_superuser=is_superuser,
            is_active=is_active,
            **extra_fields
        )
        user.set_password(password)
        user.save(using=self.db)
        return user
    
    def create_user(self, email, password=None, **extra_fields):
        return self._create_user(email, password, False, False, False, **extra_fields)

    def create_superuser(self, email, password=None, **extra_fields):
        return self._create_user(email, password, True, True, True, **extra_fields)
    
    def cut_email(self, user_fromViews):
        
        email_first = self.annotate(first_letter=Left('email',2)).get(id=user_fromViews)
        email_last = self.annotate(last_letter=Right('email',9)).get(id=user_fromViews)

        return "{}****{}".format(email_first.first_letter, email_last.last_letter)