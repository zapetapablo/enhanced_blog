from apps.home.models import Home
from apps.users.models import User

def home_contact(request):
    home = Home.objects.latest('created')
    
    return {
        'email': home.contact_email,
        'phone': home.phone,
        'avatar': home.avatar_image_default,
        'about_title': home.about_title,
    }

def profile_image(request):
    # print("------------------------------")
    # print(request.user.is_anonymous)
    home = Home.objects.latest('created')

    if not request.user.is_anonymous:
        user = User.objects.get(email=request.user)
        if user.avatar_image:
            return {
                
                'image_profile': user.avatar_image,
            }
        else:
            return {'image_profile': home.avatar_image_default,}
    else:
        return{
            'image_profile': home.avatar_image_default,
        }