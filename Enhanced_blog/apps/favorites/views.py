from django.shortcuts import render
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy, reverse
from django.http import HttpResponseRedirect



from django.views.generic import (
    ListView,
    View,
    DeleteView
    )

from .models import Favorite
from apps.users.models import User
from apps.entries.models import Entry


class FavoritesListView(LoginRequiredMixin, ListView):
    template_name = "favorites/profile.html"
    context_object_name = "detail_account"
    login_url = reverse_lazy('users_app:user-login')

    def get_context_data(self, **kwargs):
        context = super(FavoritesListView, self).get_context_data(**kwargs)
        email = User.objects.cut_email(self.request.user.id)
        # email = User.objects.annotate(last_letter=Right('email',9)).get(id=self.request.user.id)
        context['emailModified'] = email
        
        return context
    
    def get_queryset(self):
        return Favorite.objects.favorites_user(self.request.user)


class AddFavoriteView(LoginRequiredMixin, View):
    login_url = reverse_lazy('users_app:user-login')

    def post(self, request, *args, **kwargs):
        request_user = self.request.user
        favorite_entry = Entry.objects.get(id=self.kwargs['pk'])

        Favorite.objects.create(
            user = request_user,
            entry = favorite_entry
        )
        return HttpResponseRedirect(
            reverse('favorites_app:profile')
        )


class FavoriteDeleteView(DeleteView):
    model = Favorite
    success_url = reverse_lazy('favorites_app:profile')
            


