from django.db import models


class favoritesManager(models.Manager):
    def favorites_user(self, user_fromViews):
        return self.filter(
            entry__public = True,
            user = user_fromViews,
        ).order_by('-created')

    
