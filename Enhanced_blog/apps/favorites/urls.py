from django.urls import path

from . import views

app_name = "favorites_app"

urlpatterns = [
    path(
        'profile',
        views.FavoritesListView.as_view(),
        name = "profile"
        ),
    path(
        'add_favorites/<pk>',
        views.AddFavoriteView.as_view(),
        name = "add-favorites"
        ),
    path(
        'deletefavorites/<pk>/',
        views.FavoriteDeleteView.as_view(),
        name = "delete-favorites"
        )
    
]
