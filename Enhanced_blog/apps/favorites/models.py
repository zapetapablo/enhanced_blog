from django.db import models
from django.conf import settings

#trhid apps
from model_utils.models import TimeStampedModel
#our Apps
from apps.entries.models import Entry
from .managers import favoritesManager

class Favorite(TimeStampedModel):
    """Model for favorites"""
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name='user_favorites',
        on_delete=models.CASCADE
        )
    entry = models.ForeignKey(
        Entry,
        related_name="entry_favorites",
        on_delete=models.CASCADE
        )
    objects = favoritesManager()
    class Meta:
        unique_together = ('user', 'entry')
        # user only save unique entry does not repeated
        verbose_name = 'Entrada faborita'
        verbose_name_plural = 'Entradas faboritas'

    def __str__(self):
        return self.entry.title
    