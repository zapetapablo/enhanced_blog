import datetime
#
from django.shortcuts import render
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy, reverse
from django.views.generic import (
    TemplateView,
    CreateView
)
#apps Entries
from apps.entries.models import Entry

#
from .models import Home
from .forms import subscriberForm, ContactForm

class HomePageView(TemplateView):
    template_name = "home/index.html"

    def get_context_data(self, **kwargs):
        context = super(HomePageView, self).get_context_data(**kwargs)
        # load home about title and text
        context["about_me"] = Home.objects.latest("created")
        #  im seeing manager file because verified if front
        #  cover and public flag set with true
        # to load image require 3 stufs: the image, in local.py MEDIA_ROOT and
        # add in file principal/urls.py  ] + static............

        # Context for Front_cover in template index.html
        context["cover"] = Entry.objects.front_cover()
        # Context for home entries
        context["home_front_cover"] = Entry.objects.home_front_covers()
        # Context for home items new
        context["new_item_home"] = Entry.objects.home_items_new()
        #send to db the subscriber
        context["subscriber"] = subscriberForm
        return context

class SuscriberCreateView(CreateView):
    form_class = subscriberForm
    success_url = '.'


class ContactCreateView(CreateView):
    form_class = ContactForm
    success_url = '.'

