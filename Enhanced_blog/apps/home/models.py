from django.db import models

# third apps
from model_utils.models import TimeStampedModel
# READ MORE ABOUT TimeStampedModel for using
# required install way pip django-model-utils
# it's not import in thirdpartyapps of base.py

class Home(TimeStampedModel):
    """Model for app home"""
    title = models.CharField(("Nombre"), max_length=50)
    description = models.TextField()
    about_title = models.CharField(("Titulo Nosotros"), max_length=50)
    about_text = models.TextField()
    contact_email = models.EmailField(("Email de Contacto"), blank = True, null = True)
    phone = models.IntegerField(("Telefono"))
    avatar_image_default = models.ImageField(("Imagen por defecto"), upload_to='avatars', blank=True)


    class Meta:
        verbose_name = "pagina Principal"
        verbose_name_plural = "pagina Principal"

    def __str__(self):
        return self.title


class Subcribers(TimeStampedModel):
    """Model for app subscriber"""
    email = models.EmailField(("Email"), max_length=254)

    class Meta:
        verbose_name = "sucriptor"
        verbose_name_plural = "sucriptores"
    
    def __str__(self):
        return self.email

class Contact(TimeStampedModel):
    """Model for app Contact"""
    full_name = models.CharField(("Nombre Completo"), max_length=60)
    email = models.EmailField(("email"), max_length=254)
    message = models.TextField(("mensaje"))

    class Meta:
        verbose_name = "Contacto"
        verbose_name_plural = "Contacto"

    def __str__(self):
        return self.full_name
    
    
