from django.contrib import admin

from .models import *

admin.site.register(Home)
admin.site.register(Subcribers)
admin.site.register(Contact)