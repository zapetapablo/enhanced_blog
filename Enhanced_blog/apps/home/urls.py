#
from django.urls import path
from . import views

app_name = 'home_app'

urlpatterns = [
    path(
        '', 
        views.HomePageView.as_view(),
        name='home',
    ),  
    path(
        'Registro_suscriptor', 
        views.SuscriberCreateView.as_view(),
        name='new_subscriber',
    ),  
    path(
        'Contactame', 
        views.ContactCreateView.as_view(),
        name='new_message',
    ),  
] 