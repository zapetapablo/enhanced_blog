from django import forms

from .models import Subcribers, Contact

class subscriberForm(forms.ModelForm):
    
    class Meta:
        model = Subcribers
        fields = (
            'email',
            )
        widgets = {
            'email': forms.EmailInput(
                attrs={
                    'placeholder': 'Correo Electronico..',
                }
            )
        }

class ContactForm(forms.ModelForm):
    class Meta:
        model = Contact
        fields=("__all__")
