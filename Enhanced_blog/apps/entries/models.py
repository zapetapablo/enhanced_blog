#python
from datetime import datetime, timedelta
#django apps
from django.db import models
from django.conf import settings
from django.template.defaultfilters import slugify
from django.urls import reverse_lazy

#third party apps
from model_utils.models import TimeStampedModel
from ckeditor_uploader.fields import RichTextUploadingField

#our apps
#managers
from .managers import EntryManager

class Category(TimeStampedModel):
    """ models for category"""
    short_name = models.CharField(("Nombre Corto"), max_length=50, unique=True)
    name = models.CharField(("Nombre"), max_length=50)

    class Meta:
        verbose_name = "Categoria"
        verbose_name_plural = "Categorias"

    def __str__(self):
        return self.name

class Tag(TimeStampedModel):
    """Tags articles"""
    name = models.CharField(("Nombre"), max_length=50)

    class Meta:
        verbose_name = "Etiqueta"
        verbose_name_plural = "Etiquetas"

    def __str__(self):
        return self.name

class Entry(TimeStampedModel):
    """ Model for entries"""
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE
        )
    category = models.ForeignKey(
        Category,
        on_delete=models.CASCADE
        )
    tag = models.ManyToManyField(Tag)
    title = models.CharField(("titulo"), max_length=200)
    resume = models.TextField(("Resumen"))
    content = RichTextUploadingField()
    public = models.BooleanField(default=False)
    image = models.ImageField(("Imagen"), upload_to='Entry')
    cover = models.BooleanField(default=False)
    in_home = models.BooleanField(default=False)

    slug = models.SlugField(editable=False, max_length=300)

    objects = EntryManager()

    class Meta:
        verbose_name = "Entrada"
        verbose_name_plural = "Entradas"

    def __str__(self):
        return "id: {} --- Slug:{}".format(self.id, self.slug)

    def get_absolute_url(self):
        return reverse_lazy(
            'urls_entries:Entry',
            kwargs={"slug": self.slug}
            )


    def save(self, *args, **kwargs):
        now = datetime.now()
        total_time = timedelta(
            hours   = now.hour,
            minutes = now.minute,
            seconds = now.second,
        )

        seconds = int(total_time.total_seconds())
        slug_unique = "%s %s" % (self.title, str(seconds))

        self.slug = slugify(slug_unique)

        super(Entry, self).save(*args, **kwargs)
