from django.shortcuts import render

from django.views.generic import (
    ListView,
    TemplateView,
    DetailView
)

from .models import Entry, Category

class EntryListView(ListView):
    template_name = "entries/blog_list.html"
    context_object_name = "entries"
    paginate_by = 6

    def get_context_data(self, **kwargs):
        context = super(EntryListView, self).get_context_data(**kwargs)
        context["get_categories"] = Category.objects.all()
        return context

    def get_queryset(self):
        kword = self.request.GET.get("kword", '')
        categories = self.request.GET.get("categories", '')
        result = Entry.objects.search_category_and_kworkd(categories, kword)
        return result

class EntryDetailView(DetailView):
    model = Entry
    template_name = "entries/Detail_Entry.html"
