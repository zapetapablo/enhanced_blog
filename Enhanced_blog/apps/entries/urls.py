from django.urls import path

from . import views

app_name = 'urls_entries'

urlpatterns = [
    path(
        'Lista_blogs/',
        views.EntryListView.as_view(),
        name = "blog_list"
        ),
    path(
        'blog/<slug>',
        views.EntryDetailView.as_view(),
        name = "Entry"
        ),
]
