from django.db import models

class EntryManager(models.Manager):
    """Process for entry"""

    def front_cover(self):
        return self.filter(
            public = True,
            cover = True,
        ).order_by("created").first()

    def home_front_covers(self):
        return self.filter(
            public = True,
            in_home = True,
        ).order_by("-created")[:4]

    def home_items_new(self):
        return self.filter(
            public = True,
            in_home = True
        ).order_by("created")[:3]

    def search_category_and_kworkd(self, categories, kword):
        if len(categories) > 0:
            return self.filter(
                public = True,
                title__icontains = kword,
                category__short_name = categories,
            ).order_by("-created")
        else:
            return self.filter(
                public = True,
                title__icontains = kword,
            )
