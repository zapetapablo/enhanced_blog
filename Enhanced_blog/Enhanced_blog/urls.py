"""Enhanced_blog URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, re_path, include
from django.conf import settings
from django.conf.urls.static import static
#SEO
from django.contrib.sitemaps.views import sitemap
from apps.home.sitemap import (
    EntrySitemap,
    Sitemap
)
urlpatterns_main = [
    path('admin/', admin.site.urls),
    re_path('', include('apps.home.urls')),
    re_path('', include('apps.users.urls')),
    re_path('', include('apps.entries.urls')),
    re_path('', include('apps.favorites.urls')),
    #urls to ckeditor
    re_path(r'^ckeditor/', include('ckeditor_uploader.urls')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

sitemaps = {
    'site':Sitemap(
        [
            'home_app:home'
        ]
    ),
    'entries': EntrySitemap,
    # se agregan mas urls aca se crea el sitemap
}

urlpatterns_sitemap = [
    path(
        'sitemap.xml/',
        sitemap,
        {
            'sitemaps': sitemaps
        },
        name='django.contrib.sitemaps.views.sitemap'
        )   
]

urlpatterns = urlpatterns_main + urlpatterns_sitemap


